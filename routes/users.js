//users
var mongo = require('mongodb');
var mongoUri = process.env.MONGOLAB_URI ||
  process.env.MONGOHQ_URL ||
  'mongodb://localhost:27017/userdb';
  

mongo.connect(mongoUri, {}, function(error, database)
    {       
            console.log("connected, db: " + database);

            db = database;

            db.addListener("error", function(error){
            console.log("Error connecting to MongoLab");

            });
});

var BSON = require('mongodb').BSONPure;


exports.checkUserLogin = function(req, res){
	console.log(req.body);
	db.collection('users', function(err, collection){
		collection.findOne({'username': req.body.username, 'password': req.body.password}, function(err, item){
			if(item){
				res.send({'_id':item._id});
			} else{
				res.status(404).send('Not found');
			}	
		});
	});
};

exports.getUserById = function(req, res){
	var id = req.params.id;
	console.log('Retrieving user: '+id);
	db.collection('users', function(err, collection){
		collection.findOne({'_id':new BSON.ObjectID(id)}, function(err, item){
			if(item){
				res.send(item);
			} else{
				res.status(404).send('Not found');
			}	
		});
	});
};

exports.addUser = function(req, res){
	var user = req.body;
	console.log('Adding user: ', + JSON.stringify(user));
	db.collection('users', function(err, collection){
		collection.insert(user, {safe: true}, function(err, result){
			if(err){
				res.send({'error':'An error has occurred'});
			}else{
				console.log('Success: '+ JSON.stringify(result[0]));
				res.send(result[0]);
			}	
		});
	});
};

exports.updateUser = function(req, res){
	var id = req.params.id;
	var user = req.body;
	console.log('Updating user: ' + id);
	console.log(JSON.stringify(user));
	
	db.collection('users', function(err, collection){
		collection.update({'_id':new BSON.ObjectID(id)}, user, {safe:true}, function(err, result){
			if(err){
				console.log('error updating user: '+ err);
				res.send({'error':'An error has occurred'});
			}else{
				console.log(''+result+' document(s) updated');
				res.send(user);
			}
		});
	});
};


exports.deleteUser = function(req, res){
	var id = req.params.id;
    console.log('Deleting user: ' + id);
    db.collection('users', function(err, collection) {
        collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred - ' + err});
            } else {
                console.log('' + result + ' document(s) deleted');
                res.send(req.body);
            }
        });
    });
};