//db.js

var db;
var mongo = require('mongodb');
var mongoUri = process.env.MONGOLAB_URI ||
  process.env.MONGOHQ_URL ||
  'mongodb://localhost:27017/coursedb';
  
exports.db = mongo.connect(mongoUri, {}, function(error, database){       
    console.log("connected, db: " + database);

    

    database.addListener("error", function(error){
    	console.log("Error connecting to MongoLab");
	});
	//module.exports = database;
});

//module.exports.db = db;