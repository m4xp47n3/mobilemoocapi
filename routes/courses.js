//courses

var mongo = require('mongodb');
var mongoUri = process.env.MONGOLAB_URI ||
  process.env.MONGOHQ_URL ||
  'mongodb://localhost:27017/coursedb';
  
mongo.connect(mongoUri, {}, function(error, database){       
    console.log("connected, db: " + database);

    db = database;

    db.addListener("error", function(error){
    	console.log("Error connecting to MongoLab");
	});
});

var BSON = require('mongodb').BSONPure;


exports.getAllCourses = function(req, res){
	db.collection('courses', function(err, collection){
		collection.find().toArray(function(err, items){
			res.send(items);
		});
	});
};

exports.getCoursesList = function(req, res){
	db.collection('courses', function(err, collection){
		collection.find().toArray(function(err, items){
			var itemsJSON = '[';
			items.forEach(function(item){
				itemsJSON = itemsJSON.concat('{\"_id\":\"' + item._id + '\", \"title\":\"' + item.title + '\"},')
			});
			itemsJSON = itemsJSON.substring(0, itemsJSON.length - 1).concat(']');
			res.contentType('application/json');
			res.send(JSON.parse(itemsJSON));
		});
	});
};

exports.getCourseById = function(req, res){
	var id = req.params.id;
	console.log('Retrieving course: '+id);
	db.collection('courses', function(err, collection){
		collection.findOne({'_id':new BSON.ObjectID(id)}, function(err, item){
			res.send(item);
		});
	});
};

exports.addCourse = function(req, res){
	var course = req.body;
	console.log('Adding course: ', + JSON.stringify(course));
	db.collection('courses', function(err, collection){
		collection.insert(course, {safe: true}, function(err, result){
			if(err){
				res.send({'error':'An error has occurred'});
			}else{
				console.log('Success: '+ JSON.stringify(result[0]));
				res.send(result[0]);
			}	
		});
	});
};

exports.updateCourse = function(req, res){
	var id = req.params.id;
	var course = req.body;
	console.log('Updating Course: ' + id);
	console.log(JSON.stringify(course));
	
	db.collection('courses', function(err, collection){
		collection.update({'_id':new BSON.ObjectID(id)}, course, {safe:true}, function(err, result){
			if(err){
				console.log('error updating course: '+ err);
				res.send({'error':'An error has occurred'});
			}else{
				console.log(''+result+' document(s) updated');
				res.send(course);
			}
		});
	});
};

exports.deleteCourse = function(req, res){
	var id = req.params.id;
    console.log('Deleting course: ' + id);
    db.collection('courses', function(err, collection) {
        collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred - ' + err});
            } else {
                console.log('' + result + ' document(s) deleted');
                res.send(req.body);
            }
        });
    });
};