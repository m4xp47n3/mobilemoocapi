// server.js
var express = require('express');
var bodyParser = require('body-parser');
var logfmt = require('logfmt');
var courses = require('./routes/courses');
var users = require('./routes/users');
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
};

var app = express();
app.use(allowCrossDomain);
app.use(logfmt.requestLogger());
app.use(bodyParser());

app.get('/',function(req,res){ res.send('hello world')});

// Courses
app.get('/api/courses', courses.getAllCourses);

app.get('/api/courses/list', courses.getCoursesList);

app.get('/api/courses/:id', courses.getCourseById);

app.post('/api/courses', courses.addCourse);

app.put('/api/courses/:id', courses.updateCourse);

app.delete('/api/courses/:id', courses.deleteCourse);


// Users
app.post('/api/users/login', users.checkUserLogin);

app.get('/api/users/:id', users.getUserById);

app.post('/api/users', users.addUser);

app.put('/api/users/:id', users.updateUser);

app.delete('/api/users/:id', users.deleteUser);


var port = Number(process.env.PORT || 5000);
app.listen(port, function() {
  	console.log('Listening on ' + port);
});